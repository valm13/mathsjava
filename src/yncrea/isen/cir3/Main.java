package yncrea.isen.cir3;

import static yncrea.isen.cir3.lib.FFTCplx.appliqueSur;

import yncrea.isen.cir3.lib.Complexe;
import yncrea.isen.cir3.lib.Son;
import yncrea.isen.cir3.neurone.Neurone;

public class Main {

	public static void main(String[] args) {
		int taille_bloc = 2048;
		double[][] donneesModulesPhoque = creeDonneesEntreeneurone(new Son("sons/cut/phoque_1.wav"),taille_bloc);
		double[][] donneesModulesBaleine1 = creeDonneesEntreeneurone(new Son("sons/cut/baleine_2.wav"),taille_bloc);
		double[][] donneesModulesBaleine2 = creeDonneesEntreeneurone(new Son("sons/cut/baleine_3.wav"),taille_bloc);
		
		// On combine les tableaux en 1 pour apprendre au neurone
		
		double[][] tabCombinee = new double[donneesModulesPhoque.length + donneesModulesBaleine1.length][taille_bloc];
		
		// Phoque
		for (int i = 0; i < donneesModulesPhoque.length; i++)
			tabCombinee[i] = donneesModulesPhoque[i];
			
		// On ajoute ceux de la baleine 1 (la Baleine 2 sera celle que l'on va tenter d'identifier)
		for (int i = 0; i < donneesModulesBaleine1.length; i++)
			tabCombinee[i+donneesModulesPhoque.length] = donneesModulesBaleine1[i];
		
		
		
		// On rajoute les sorties que le neurone doit apprendre
		// Sorties du phoque : 0 car on veut qu'il apprenne la baleine
		int []sortiesEsperees1 = new int [donneesModulesPhoque.length];
		for (int i = 0; i < donneesModulesPhoque.length; i++)
			sortiesEsperees1[i] = 0;
		
		// Sorties de la baleine : 1 car on veut qu'il apprenne la baleine
		int []sortiesEsperees2 = new int [donneesModulesBaleine1.length];
		for (int i = 0; i < donneesModulesBaleine1.length; i++)
			sortiesEsperees2[i] = 1;
		
		
		
		int []sortiesEspereesCombine = new int [sortiesEsperees1.length+sortiesEsperees2.length];
		
		for (int i = 0; i < sortiesEsperees1.length; i++)
			sortiesEspereesCombine[i] = sortiesEsperees1[i];
		for (int i = 0; i < sortiesEsperees2.length; i++)
			sortiesEspereesCombine[i+sortiesEsperees1.length] = sortiesEsperees2[i];
		
		
		
		Neurone n = new Neurone(taille_bloc);
		System.out.println("--- Apprentissage de la baleine  ---");
		n.apprend(tabCombinee, sortiesEspereesCombine);

		
		System.out.println("--- Calcul pour la baleine ---");
		int nbBaleine = 0;
		double pourcentage;
		for(int i = 0; i < donneesModulesBaleine2.length; i++) {
			if (n.calculSortie(donneesModulesBaleine2[i]) == 1)
			{
				System.out.println("C'est une baleine");
				nbBaleine++;
				
			}
				
			else
				System.out.println("C'est autre chose");
		}
		pourcentage = 100*(double)nbBaleine/donneesModulesBaleine2.length;
		System.out.println("Il y a "+pourcentage+"% de chance que ce soit une baleine");
	}
	
	public static Complexe[][] FFTBlocs(Complexe[][] Blocs)
	{
		// Initialisation du tableaux résultant de toutes les FFT
		Complexe[][] FFTResult = new Complexe[Blocs.length][Blocs[0].length];
		
		// On applique la FFT sur chaque bloc
		for(int i = 0; i < Blocs.length; i++)
		{
			FFTResult[i] = appliqueSur(Blocs[i]);
//			System.out.println("YES");
		}
		return FFTResult;
	}
	
	public static double[] recupModule(Complexe[] BlocFFT)
	{
		double[] TabModule = new double[BlocFFT.length];
		for (int i = 0; i < BlocFFT.length; i++)
		{
			TabModule[i] = BlocFFT[i].mod();
		}
		return TabModule;
	}
	
	public static int trouveTailleTabPuissanceDe2(int length)
	{
		int newLenght = 1;
		int puissance = 0;
		while(newLenght < length)
		{
			newLenght = 2*newLenght;
			puissance++;
		}
		System.out.println("2^"+puissance);
		return newLenght;
	}
	
	public static  float[] remplisTab(float[] dataIn)
	{
		// On cherche le 2^N tel que 2^N supérieur à la longueur du tableau.
		int length = trouveTailleTabPuissanceDe2(dataIn.length);
		
		float[] tab = new float[length];
		
		// On remplit le nouveau tableau avec les données sonores
		for(int i = 0; i < dataIn.length; i++)
		{
			tab[i] = dataIn[i];
		}
		// On complète le nouveau tableau avec des 0
		for(int i = dataIn.length; i < length; i++)
		{
			tab[i] = 0;
		}		
		return tab;		
	}
	
	public static double[][] creeDonneesEntreeneurone(Son s, int taille_bloc){
		Complexe[][] TabBlocs = s.decoupeSonEnBlocsComplexes(taille_bloc);
		Complexe[][] TabBlocsFFT = FFTBlocs(TabBlocs);
		double[][] TabModule = new double[TabBlocsFFT.length][taille_bloc];
		for (int i = 0; i < TabBlocsFFT.length; i++) {
			 TabModule[i] = recupModule(TabBlocsFFT[i]);
		}
		return TabModule;
	}
}
