package yncrea.isen.cir3.neurone;

import java.util.Random;

public class Neurone {
	
	private double[] poids;
	private Biais biais;
	private double speed;
	
	public Neurone(Biais biais, double speed, int nbEntrees) {
		Random r = new Random();
		
		this.biais = biais;
		this.speed = speed;
		
		this.poids = new double[nbEntrees];
		for(int i = 0; i <nbEntrees; i++)
		{
			poids[i] = -1 + (1 - -1) * r.nextDouble();
		}
	}
	
	public Neurone(int nbEntrees) {
		this(new Biais(),0.10,nbEntrees);
	}
	
	public int calculSortie(double[] entrees) {
		double somme = 0.0;
		
		// On calcul le produit poid*entrées
		for(int i = 0; i < entrees.length; i++)
		{
			somme+=entrees[i]*poids[i];
		}
		
		// On ajoute le biais
		somme+= biais.evalueBiais();
//		System.out.println(somme);
		// On éxécute la fonction de seuil et renvoie la valeur
		return seuil(somme);
	}
	
	private int seuil(double n)
	{
		return n >= 0 ? 1 : 0; 
	}
	
	private void modifiePoids(int sortiesEsperee, int sortieObtenu, double[] entrees)
	{
		// On modifie les poids des entrées :
		for(int i= 0; i < poids.length; i++)
		{
			poids[i] = poids[i] + speed*entrees[i]*(sortiesEsperee-sortieObtenu);
		}
		// On modifie la valeur du biais :
		double nouveauBiais = biais.getValue() + speed*biais.getSign()*(sortiesEsperee-sortieObtenu);
		biais.setValue(nouveauBiais);
	}
	
	public void apprend(double[][] entrees, int[] sortiesEsperee)
	{
		int nombreDeTest = entrees.length;
		int validation = 0;
		int sortieObtenu = 0;
		int nbApprentissage = 0;
		
		if(nombreDeTest == sortiesEsperee.length)
		{
			// Tant qu'on ne valide pas Tout les tests d'affilé
			while(validation < nombreDeTest) 
			{
				// On effectue chaque test
				validation = 0;
				for(int i= 0; i < nombreDeTest; i++)
				{
					// On calcul la sortie du vecteur d'entrees
					sortieObtenu = calculSortie(entrees[i]);
					
					// Si on a ce que l'on attendait, on passe au prochain couple vecteur d'entrées/sortie
					if(sortieObtenu == sortiesEsperee[i])
						validation++;
					
					// Sinon on passe en phase d'apprentissage, et on repasse le compteur à 0
					else
					{
//						System.out.println("Espéré : "+sortiesEsperee[i]+", obtenu : "+sortieObtenu);
						modifiePoids(sortiesEsperee[i], sortieObtenu,entrees[i]);
//						for(int j=0;j<poids.length;j++)
//							System.out.print(poids[j]+" ");
//						System.out.println("");
						nbApprentissage++;
					}
				}
			}
			System.out.println("Nombre d'apprentissage = "+nbApprentissage);
			
		}
		else
			System.out.println("ERREUR : Le nombre de test est différent du nombre de sorties espérées");				
	}
	
}
