package yncrea.isen.cir3.neurone;

public class TestNeuronePorteETOU {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double entree1[] = new double[2];
		entree1[0] = 0;
		entree1[1] = 0;
		
		double entree2[] = new double[2];
		entree2[0] = 0;
		entree2[1] = 1;
		
		double entree3[] = new double[2];
		entree3[0] = 1;
		entree3[1] = 0;
		
		double entree4[] = new double[2];
		entree4[0] = 1;
		entree4[1] = 1;
		
		double entrees[][] = new double[4][2];
		entrees[0] = entree1;
		entrees[1] = entree2;
		entrees[2] = entree3;
		entrees[3] = entree4;
		
		// On prépare les sorties à apprendre
		int sortiesET[] = new int[4];
		for(int i = 0; i < 3; i++)
			sortiesET[i] = 0;
		sortiesET[3] = 1;
		
		int sortiesOU[] = new int[4];
		sortiesOU[0] = 0;
		for(int i = 1; i < 4; i++)
			sortiesOU[i] = 1;
		
		Neurone n = new Neurone(2);
		
		// On vérifie pour un OU logique
		System.out.println("--- Apprentissage du OU au neurone formel ---");
		n.apprend(entrees, sortiesOU);
		System.out.println("--- Affichage du OU apris ---");
		for(int i = 0; i < 4; i++)
			System.out.println("Entrée : ["+entrees[i][0]+"]["+entrees[i][1]+"] --> sortie : "+n.calculSortie(entrees[i]));
				
		// On vérifie pour un ET logique
		System.out.println("--- Apprentissage du ET au neurone formel ---");
		n.apprend(entrees, sortiesET);
		System.out.println("--- Affichage du ET apris ---");
		for(int i = 0; i < 4; i++)
			System.out.println("Entrée : ["+entrees[i][0]+"]["+entrees[i][1]+"] --> sortie : "+n.calculSortie(entrees[i]));
		
		
		
		
		
		
	}

}
