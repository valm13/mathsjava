package yncrea.isen.cir3.neurone;

public class Biais {
	
	private double value;
	private int sign;
	
	public Biais() {
		this(1);
	}
	
	public Biais(int sign) {
		this.sign = sign;
		this.value = Math.random();
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getSign() {
		return sign;
	}
	
	public double evalueBiais() {
		return sign >= 0 ? value : -value;
	}
	

}
